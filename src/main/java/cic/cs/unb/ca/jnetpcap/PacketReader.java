package cic.cs.unb.ca.jnetpcap;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapClosedException;
import org.jnetpcap.PcapHeader;
import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.nio.JMemory;
import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JHeaderPool;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.network.Ip6;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;
import org.jnetpcap.protocol.vpn.L2TP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this classe allows to get info about packet in given pcap files
 *
 */
public class PacketReader {

	private static final Logger logger = LoggerFactory.getLogger(PacketReader.class);
	private IdGenerator  generator = new IdGenerator(); // PacketReader id
	private Pcap pcapReader; //the file
	
	private long firstPacket; // the first packet
	private long lastPacket; // the last packet
	
	private Tcp  tcp;
	private Udp  udp;
	private Ip4  ipv4;
	private Ip6  ipv6;
	private L2TP l2tp;
	private PcapHeader hdr;
	private JBuffer buf;
	
	private boolean readIP6; // boolean if IPV6 should be reed
	private boolean readIP4; // boolean if IPV4 should be reed
	private String file; // access to the file

	/**
	 * constructor with a file name
	 * we set IPV4 to true and IPV6 to false
	 * @param filename the file to be scanned
	 * @see	#config(String filename)
	 */
	public PacketReader(String filename) {
		super();	
		this.readIP4 = true;
		this.readIP6 = false;		
		this.config(filename);
	}

	/**
	 * contructor with file name, that allows to read  IPV4 or IPV6
	 * @param filename file to be read
	 * @param readip4 true if we take IPV4 packet
	 * @param readip6 true if we take IPV6 packet
	 * @see	#config(String filename)
	 */
	public PacketReader(String filename, boolean readip4, boolean readip6) {
		super();	
		this.readIP4 = readip4;
		this.readIP6 = readip6;
		this.config(filename);
	}

	/**
	 * config the packet reader with the given filename
	 * set the local file as the file pointed by filename
	 * generate tcp, udp, ipv4 , ipv6 , l2tp
	 * set firstPacket and lastpaket to 0L
	 * @param filename name of the file to be reed
	 */
	private void config(String filename){
        file = filename;
		StringBuilder errbuf = new StringBuilder(); // For error messages
		pcapReader = Pcap.openOffline(filename, errbuf);
		
		this.firstPacket = 0L;
		this.lastPacket = 0L;

		if (pcapReader == null) {
			logger.error("Error while opening file for capture: "+errbuf.toString());
			System.exit(-1);
		}else{
			this.tcp = new Tcp();
			this.udp = new Udp();
			this.ipv4 = new Ip4();
			this.ipv6 = new Ip6();
			this.l2tp = new L2TP();
			hdr = new PcapHeader(JMemory.POINTER);
			buf = new JBuffer(JMemory.POINTER);		
		}		
	}

	/**
	 * gets the global info of the next packet of the pcap
	 * checks for readIP4 and use ivp4info method to get the info
	 * @see #getIpv4Info(PcapPacket paket )
	 * else checks for readIP6 and use ipv6info method to get the info
	 * @see #getIpv6Info(PcapPacket paket)
	 * if there is no info givent by ipv4 and ipv6 methode it uses VPNInfo
	 * @see #getVPNInfo(PcapPacket paket)
	 * @return BasicPacketInfo that contains infos about the packet
	 */
	public BasicPacketInfo nextPacket(){
		 PcapPacket      packet; // will contain next packet
		 BasicPacketInfo packetInfo = null; // will contain next packet info
		 try{
			 if(pcapReader.nextEx(hdr,buf) == Pcap.NEXT_EX_OK){ //try to find the next packet
				 packet = new PcapPacket(hdr, buf);
				 packet.scan(Ethernet.ID);				 
				 // packet contain the packet that will be analyse
				 if(this.readIP4){ // if we should read ipv4
					 packetInfo = getIpv4Info(packet); // get the ipv4 infos of the packet
					 if (packetInfo == null && this.readIP6){ // if there is no ipv4 infos and we also want ipv6 we get ipv6 infos
					 	packetInfo = getIpv6Info(packet); // get the ipv6 infos
					 }					 
				 }else if(this.readIP6){ // otherwise check if we need ipv6
					 packetInfo = getIpv6Info(packet); // get the ipv6 info of the packet
					 if (packetInfo == null && this.readIP4){ // if there is no ipv6 infos and we also want ipv4 we get ipv4 infos
					 	packetInfo = getIpv4Info(packet); //get the ipv4 infos
					 }
				 }
				 
				 if (packetInfo == null){ // if there is no infos
					 packetInfo = getVPNInfo(packet); // get the vpn infos
				 }					 

			 }else{
				 throw new PcapClosedException();
			 }
		 }catch(PcapClosedException e){
			 logger.debug("Read All packets on {}",file);
			 throw e;
		 }catch(Exception ex){
			 logger.debug(ex.getMessage());
		 }
		 return packetInfo;
	}

	/**
	 * Get the info of a paket if it's an ipv4 packet 
	 * else return null
	 * @param packet packet to be scanned 
	 * @return BasicPacketInfo or null
	 */
	private BasicPacketInfo getIpv4Info(PcapPacket packet){
		BasicPacketInfo packetInfo = null; // infos about the packet
		try {
			if (packet.hasHeader(ipv4)){ //check if the packet is an ipv4 packet
				packetInfo = new BasicPacketInfo(this.generator); // generate a packet info generator
				packetInfo.setSrc(this.ipv4.source()); 
				packetInfo.setDst(this.ipv4.destination());
				//packetInfo.setTimeStamp(packet.getCaptureHeader().timestampInMillis());
				packetInfo.setTimeStamp(packet.getCaptureHeader().timestampInMicros());
				
				if(this.firstPacket == 0L) // if it is the first packet 
					this.firstPacket = packet.getCaptureHeader().timestampInMillis();// set the first packet as this packet
				this.lastPacket = packet.getCaptureHeader().timestampInMillis(); // the curent packet is the last packet we prossessed
				// treat the packet in function of the protocol and set the right packet info
				if(packet.hasHeader(this.tcp)){ // it the packet protocol is tcp
					packetInfo.setTCPWindow(tcp.window());
					packetInfo.setSrcPort(tcp.source());
					packetInfo.setDstPort(tcp.destination());
					packetInfo.setProtocol(6);
					packetInfo.setFlagFIN(tcp.flags_FIN());
					packetInfo.setFlagPSH(tcp.flags_PSH());
					packetInfo.setFlagURG(tcp.flags_URG());
					packetInfo.setFlagSYN(tcp.flags_SYN());
					packetInfo.setFlagACK(tcp.flags_ACK());
					packetInfo.setFlagECE(tcp.flags_ECE());
					packetInfo.setFlagCWR(tcp.flags_CWR());
					packetInfo.setFlagRST(tcp.flags_RST());
					packetInfo.setPayloadBytes(tcp.getPayloadLength());
					packetInfo.setHeaderBytes(tcp.getHeaderLength());
				}else if(packet.hasHeader(this.udp)){// if the packet protocol is udp
					packetInfo.setSrcPort(udp.source());
					packetInfo.setDstPort(udp.destination());
					packetInfo.setPayloadBytes(udp.getPayloadLength());
					packetInfo.setHeaderBytes(udp.getHeaderLength());
					packetInfo.setProtocol(17);			
				}else {
					/*logger.debug("other packet Ethernet -> {}"+  packet.hasHeader(new Ethernet()));
					logger.debug("other packet Html     -> {}"+  packet.hasHeader(new Html()));
					logger.debug("other packet Http     -> {}"+  packet.hasHeader(new Http()));
					logger.debug("other packet SLL      -> {}"+  packet.hasHeader(new SLL()));
					logger.debug("other packet L2TP     -> {}"+  packet.hasHeader(new L2TP()));
					logger.debug("other packet Sctp     -> {}"+  packet.hasHeader(new Sctp()));
					logger.debug("other packet PPP      -> {}"+  packet.hasHeader(new PPP()));*/

					int headerCount = packet.getHeaderCount();
					for(int i=0;i<headerCount;i++) {
						JHeader header = JHeaderPool.getDefault().getHeader(i);
						//JHeader hh = packet.getHeaderByIndex(i, header);
						//logger.debug("getIpv4Info: {} --description: {} ",header.getName(),header.getDescription());
					}
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			packet.scan(ipv4.getId());
			String errormsg = "";
			errormsg+=e.getMessage()+"\n";
			//errormsg+=packet.getHeader(new Ip4())+"\n";
			errormsg+="********************************************************************************"+"\n";
			errormsg+=packet.toHexdump()+"\n";
			
			//System.out.println(errormsg);
			logger.debug(errormsg);
			//System.exit(-1);
			return null;
		}
		
		return packetInfo;
	}

	/**
	 * return the packet info if the paket is an IPV6 
	 * packet else return null
	 * @param packet the packet to be scanned
	 * @return the packets info or null
	 */
	private BasicPacketInfo getIpv6Info(PcapPacket packet){
		BasicPacketInfo packetInfo = null;
		try{
			if(packet.hasHeader(ipv6)){ // check if the packet is an ipv6 packet
				packetInfo = new BasicPacketInfo(this.generator);
				packetInfo.setSrc(this.ipv6.source());
				packetInfo.setDst(this.ipv6.destination());
				packetInfo.setTimeStamp(packet.getCaptureHeader().timestampInMillis());			
				if(packet.hasHeader(this.tcp)){	// set the packet infos if the packet protocol is tcp
					packetInfo.setSrcPort(tcp.source());
					packetInfo.setDstPort(tcp.destination());
					packetInfo.setPayloadBytes(tcp.getPayloadLength());
					packetInfo.setHeaderBytes(tcp.getHeaderLength());
					packetInfo.setProtocol(6);
				}else if(packet.hasHeader(this.udp)){	// set the packet infos if the packet protocol is udp
					packetInfo.setSrcPort(udp.source());
					packetInfo.setDstPort(udp.destination());
					packetInfo.setPayloadBytes(udp.getPayloadLength());
					packetInfo.setHeaderBytes(tcp.getHeaderLength());
					packetInfo.setProtocol(17);								
				}		
			}
		}catch(Exception e){
			logger.debug(e.getMessage());
			packet.scan(ipv6.getId());
			String errormsg = "";
			errormsg+=e.getMessage()+"\n";
			//errormsg+=packet.getHeader(new Ip6())+"\n";
			errormsg+="********************************************************************************"+"\n";
			errormsg+=packet.toHexdump()+"\n";
			
			//System.out.println(errormsg);
			logger.debug(errormsg);
			//System.exit(-1);
			return null;			
		}
				
		return packetInfo;
	}

	/**
	 * scans the packet using L2TP scan
	 * @param packet the packet to be scanned
	 * @return the packet infos or null
	 */
	private BasicPacketInfo getVPNInfo(PcapPacket packet){		
		BasicPacketInfo packetInfo = null;		
		try {
			packet.scan(L2TP.ID);
			
			if (packet.hasHeader(l2tp)){				
		    	if(this.readIP4){		
		    		packet.scan(ipv4.getId());
		    		packetInfo = getIpv4Info(packet);
		    		if (packetInfo == null && this.readIP6){
		    			packet.scan(ipv6.getId());
		    			packetInfo = getIpv6Info(packet);				 	
		    		}					 
		    	}else if(this.readIP6){
		    		packet.scan(ipv6.getId());
		    		packetInfo = getIpv6Info(packet);
		    		if (packetInfo == null && this.readIP4){
		    			packet.scan(ipv4.getId());
		    			packetInfo = getIpv4Info(packet);
		    		}
		    	}				

			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
			packet.scan(l2tp.getId());
			String errormsg = "";
			errormsg+=e.getMessage()+"\n";
			//errormsg+=packet.getHeader(new L2TP())+"\n";
			errormsg+="********************************************************************************"+"\n";
			errormsg+=packet.toHexdump()+"\n";
			
			//System.out.println(errormsg);
			logger.debug(errormsg);
			//System.exit(-1);
			return null;
		}
		
		return packetInfo;
	}	

	public long getFirstPacket() {
		return firstPacket;
	}

	public void setFirstPacket(long firstPacket) {
		this.firstPacket = firstPacket;
	}

	public long getLastPacket() {
		return lastPacket;
	}

	public void setLastPacket(long lastPacket) {
		this.lastPacket = lastPacket;
	}	

	/*
	 * So far, the value of the field BasicPacketInfo.id is not used
	 * It doesn't matter just using a static IdGenerator for realtime PcapPacket reading
	 */
	private static IdGenerator idGen = new IdGenerator();

	/**
	 * get the info of the packet :
	 * get ipv4info if readIP4 is true
	 * @see #getIpv4Info(PcapPacket, Protocol)
	 * get ipv6info if readIPv6 is true
	 * @see #getIpv6Info(PcapPacket, Protocol)
	 * @param packet the packet to be scanned
	 * @param readIP4 if we want ipv4 info
	 * @param readIP6 if we want ipv6 info
	 * @return BasicPacketInfo if we successfully got infos else null
	 */
	public static BasicPacketInfo getBasicPacketInfo(PcapPacket packet,boolean readIP4, boolean readIP6) {
		BasicPacketInfo packetInfo = null;
		Protocol protocol = new Protocol();
		if(readIP4){ // if ipv4 is to be read 
			packetInfo = getIpv4Info(packet,protocol); // we get ipv4 packets infos
			if (packetInfo == null && readIP6){ // if packetInfo is null and ipv6 is to be read 
				packetInfo = getIpv6Info(packet,protocol);	 // we get ipv6 packets infos			 	
			}					 
		}else if(readIP6){ // else if ipv6 is to be read 
			packetInfo = getIpv6Info(packet,protocol); // we get ipv6 packets infos
			if (packetInfo == null && readIP4){ // if packetInfo is null and ipv4 is to be read 
				packetInfo = getIpv4Info(packet,protocol); // we get ipv4 packets infos
			}
		}
		
		if (packetInfo == null){
			packetInfo = getVPNInfo(packet,protocol,readIP4,readIP6);
		}
		
		return packetInfo;
	}

	/**
	 * Scans the packet with L2tp scan
	 * if the packet has a l2tp protocol tries to get ipv4 or ipv6 infos
	 * @param packet the packet to be read
	 * @param protocol the packet protocol
	 * @param readIP4 if ipv4 info should be reed
	 * @param readIP6 if ipv6 info should be reed
	 * @return BasicPacketInfo packetInfo if we sucesfully got infos else null
	 */
	private static BasicPacketInfo getVPNInfo(PcapPacket packet,Protocol protocol,boolean readIP4, boolean readIP6) {		
		BasicPacketInfo packetInfo = null;
		try {
			packet.scan(L2TP.ID);
			
			if (packet.hasHeader(protocol.getL2tp())){
		    	if(readIP4){		
		    		packet.scan(protocol.getIpv4().getId());
		    		packetInfo = getIpv4Info(packet,protocol);
		    		if (packetInfo == null && readIP6){
		    			packet.scan(protocol.getIpv6().getId());
		    			packetInfo = getIpv6Info(packet,protocol);				 	
		    		}					 
		    	}else if(readIP6){
		    		packet.scan(protocol.getIpv6().getId());
		    		packetInfo = getIpv6Info(packet,protocol);
		    		if (packetInfo == null && readIP4){
		    			packet.scan(protocol.getIpv4().getId());
		    			packetInfo = getIpv4Info(packet,protocol);
		    		}
		    	}				

			}
		} catch (Exception e) {
			/*
			 * BufferUnderflowException while decoding header
			 * havn't fixed, so do not e.printStackTrace() 
			 */
			//e.printStackTrace();
			/*packet.scan(protocol.l2tp.getId());
			String errormsg = "";
			errormsg+=e.getMessage()+"\n";
			//errormsg+=packet.getHeader(new L2TP())+"\n";
			errormsg+="********************************************************************************"+"\n";
			errormsg+=packet.toHexdump()+"\n";
			logger.error(errormsg);*/
			return null;
		}
		
		return packetInfo;
	}

	/**
	 * get the ipv6 packet info if the packet is an ipv6 packet else return null
	 * @param packet the packet to be read
	 * @param protocol transmission protocol of the packet
	 * @return BasicPacketInfo packetInfo if we successfully got info else null
	 */
	private static BasicPacketInfo getIpv6Info(PcapPacket packet,Protocol protocol) {
		BasicPacketInfo packetInfo = null;
		try{
			if(packet.hasHeader(protocol.getIpv6())){
				packetInfo = new BasicPacketInfo(idGen);
				packetInfo.setSrc(protocol.getIpv6().source());
				packetInfo.setDst(protocol.getIpv6().destination());
				packetInfo.setTimeStamp(packet.getCaptureHeader().timestampInMillis());			
				
				if(packet.hasHeader(protocol.getTcp())){
					packetInfo.setSrcPort(protocol.getTcp().source());
					packetInfo.setDstPort(protocol.getTcp().destination());
					packetInfo.setPayloadBytes(protocol.getTcp().getPayloadLength());
					packetInfo.setHeaderBytes(protocol.getTcp().getHeaderLength());
					packetInfo.setProtocol(6);
				}else if(packet.hasHeader(protocol.getUdp())){
					packetInfo.setSrcPort(protocol.getUdp().source());
					packetInfo.setDstPort(protocol.getUdp().destination());
					packetInfo.setPayloadBytes(protocol.getUdp().getPayloadLength());
					packetInfo.setHeaderBytes(protocol.getUdp().getHeaderLength());
					packetInfo.setProtocol(17);								
				}		
			}
		}catch(Exception e){
			/*
			 * BufferUnderflowException while decoding header
			 * havn't fixed, so do not e.printStackTrace()
			 */
			//e.printStackTrace();
			/*packet.scan(protocol.ipv6.getId());
			String errormsg = "";
			errormsg+=e.getMessage()+"\n";
			//errormsg+=packet.getHeader(new Ip6())+"\n";
			errormsg+="********************************************************************************"+"\n";
			errormsg+=packet.toHexdump()+"\n";
			logger.error(errormsg);
			//System.exit(-1);*/
			return null;			
		}
				
		return packetInfo;
	}

	/**
	 * get the ipv4 infos is the packet is an ipv4 packet else return null
	 * @param packet packet to be read
	 * @param protocol transmission protocol of the packet
	 * @return BasicPacketInfo packetInfo if we successfully got infos else null
	 */
	private static BasicPacketInfo getIpv4Info(PcapPacket packet,Protocol protocol) {
		BasicPacketInfo packetInfo = null;		
		try {
						
			if (packet.hasHeader(protocol.getIpv4())){
				packetInfo = new BasicPacketInfo(idGen);
				packetInfo.setSrc(protocol.getIpv4().source());
				packetInfo.setDst(protocol.getIpv4().destination());
				//packetInfo.setTimeStamp(packet.getCaptureHeader().timestampInMillis());
				packetInfo.setTimeStamp(packet.getCaptureHeader().timestampInMicros());
				
				/*if(this.firstPacket == 0L)
					this.firstPacket = packet.getCaptureHeader().timestampInMillis();
				this.lastPacket = packet.getCaptureHeader().timestampInMillis();*/

				if(packet.hasHeader(protocol.getTcp())){
					packetInfo.setTCPWindow(protocol.getTcp().window());
					packetInfo.setSrcPort(protocol.getTcp().source());
					packetInfo.setDstPort(protocol.getTcp().destination());
					packetInfo.setProtocol(6);
					packetInfo.setFlagFIN(protocol.getTcp().flags_FIN());
					packetInfo.setFlagPSH(protocol.getTcp().flags_PSH());
					packetInfo.setFlagURG(protocol.getTcp().flags_URG());
					packetInfo.setFlagSYN(protocol.getTcp().flags_SYN());
					packetInfo.setFlagACK(protocol.getTcp().flags_ACK());
					packetInfo.setFlagECE(protocol.getTcp().flags_ECE());
					packetInfo.setFlagCWR(protocol.getTcp().flags_CWR());
					packetInfo.setFlagRST(protocol.getTcp().flags_RST());
					packetInfo.setPayloadBytes(protocol.getTcp().getPayloadLength());
					packetInfo.setHeaderBytes(protocol.getTcp().getHeaderLength());
				}else if(packet.hasHeader(protocol.getUdp())){
					packetInfo.setSrcPort(protocol.getUdp().source());
					packetInfo.setDstPort(protocol.getUdp().destination());
					packetInfo.setPayloadBytes(protocol.getUdp().getPayloadLength());
					packetInfo.setHeaderBytes(protocol.getUdp().getHeaderLength());
					packetInfo.setProtocol(17);			
				} else {
					int headerCount = packet.getHeaderCount();
					for(int i=0;i<headerCount;i++) {
						JHeader header = JHeaderPool.getDefault().getHeader(i);
						//JHeader hh = packet.getHeaderByIndex(i, header);
						//logger.debug("getIpv4Info: {} --description: {} ",header.getName(),header.getDescription());
					}
				}
			}
		} catch (Exception e) {
			/*
			 * BufferUnderflowException while decoding header
			 * havn't fixed, so do not e.printStackTrace()
			 */
			//e.printStackTrace();
			/*packet.scan(protocol.ipv4.getId());
			String errormsg = "";
			errormsg+=e.getMessage()+"\n";
			//errormsg+=packet.getHeader(new Ip4())+"\n";
			errormsg+="********************************************************************************"+"\n";
			errormsg+=packet.toHexdump()+"\n";
			logger.error(errormsg);
			return null;*/
		}
		
		return packetInfo;
	}
}

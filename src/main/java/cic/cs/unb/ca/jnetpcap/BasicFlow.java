package cic.cs.unb.ca.jnetpcap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.jnetpcap.packet.format.FormatUtils;

/**
 * Structure of a flow
 * allow to stock a list of packet and to generate the features
 */
public class BasicFlow {
	/**
	 * output separator
	 */
	private final static String separator = ",";
	/**
	 * summarise the statistics on all the forward packets
	 */
	private     SummaryStatistics 		fwdPktStats = null;
	/**
	 * summarise the statistics on all the backward packets
	 */
	private		SummaryStatistics 		bwdPktStats = null;
	/**
	 * list of the infos of forward packets
	 */
	private 	List<BasicPacketInfo> 	forward = null;
	/**
	 * List of the infos of backward packet
	 */
	private		List<BasicPacketInfo> 	backward = null;
	/**
	 * counter of the number of bytes in forward direction
	 */
	private 	long forwardBytes;
	/**
	 * counter of the number of bytes in backward direction
	 */
	private 	long backwardBytes;
	/**
	 * counter of header size in byte in forward direction
	 */
	private 	long fHeaderBytes;
	/**
	 * counter of header size in byte in forward direction
	 */
	private 	long bHeaderBytes;
	/**
	 * boolean true if flows are bidirectional false if they are not
	 */
	private 	boolean isBidirectional;
	/**
	 * Contain all accessible flag and the number of packet with those flag in the flow
	 */
	private 	HashMap<String, MutableInt> flagCounts;

	private 	int fPSH_cnt;
	private 	int bPSH_cnt;
	private 	int fURG_cnt;
	private 	int bURG_cnt;

	private 	long Act_data_pkt_forward;
	private 	long min_seg_size_forward;
	private 	int Init_Win_bytes_forward=0;
	private 	int Init_Win_bytes_backward=0;

	/**
	 * source address
	 */
	private		byte[] src;
	/**
	 * destination address
	 */
    private    	byte[] dst;
	/**
	 * source port
	 */
	private    	int    srcPort;
	/**
	 * destination port
	 */
    private    	int    dstPort;
	/**
	 * transmission protocol
	 */
	private    	int    protocol;
    private    	long   flowStartTime;
	/**
	 * time when the flow start
	 */
	private    	long   startActiveTime;
	/**
	 * last time when the flow was active
	 */
    private    	long   endActiveTime;
	/**
	 * flow identifier
	 */
	private    	String flowId = null;
	/**
	 *  Statistics on iat flow
	 */
	private     SummaryStatistics flowIAT = null;
	/**
	 * Statistics on iat forward flow
	 */
    private     SummaryStatistics forwardIAT = null;
	/**
	 * Statistics on iat backwar flow
	 */
	private     SummaryStatistics backwardIAT = null;
	/**
	 *  Statistics on the lenght of flows
	 */
	private     SummaryStatistics flowLengthStats = null;
	/**
	 *  Statistics on active flows
	 */
    private     SummaryStatistics flowActive = null;
	/**
	 *  Statistics on flows becoming inactive
	 */
	private     SummaryStatistics flowIdle = null;
    
    private	    long   flowLastSeen;
    private     long   forwardLastSeen;
    private     long   backwardLastSeen;

	/**
	 * Constructor
	 * Create a new BasicFlow
	 * @param isBidirectional true if the flow is considered as bidirectional
	 * @param packet first packet of the flow
	 * @param flowSrc source address of the flow
	 * @param flowDst destination address of the flow
	 * @param flowSrcPort source port of the flow
	 * @param flowDstPort destination port of the flow
	 */
	public BasicFlow(boolean isBidirectional,BasicPacketInfo packet, byte[] flowSrc, byte[] flowDst, int flowSrcPort, int flowDstPort) {
		super();
		this.initParameters();
		this.isBidirectional = isBidirectional;
		this.firstPacket(packet);
		this.src = flowSrc;
		this.dst = flowDst;
		this.srcPort = flowSrcPort;
		this.dstPort = flowDstPort;
	}

	/**
	 * Constructor
	 * allow to construct without a first packet and without source/destination address and port
	 * @param isBidirectional if the flow is considered as bidirectional
	 * @param packet packet add as the first packet
	 */
	public BasicFlow(boolean isBidirectional,BasicPacketInfo packet) {
		super();
		this.initParameters();
		this.isBidirectional = isBidirectional;
		this.firstPacket(packet);
	}

	/**
	 * never used constructor
	 * @param packet first_packet
	 */
	public BasicFlow(BasicPacketInfo packet) {
		super();
		this.initParameters();
		this.isBidirectional = true;		
		firstPacket(packet);
	}

	/**
	 * initialise all SummaryStatistics and few other attributes
	 */
	public void initParameters(){
		this.forward = new ArrayList<BasicPacketInfo>();
		this.backward = new ArrayList<BasicPacketInfo>();
		this.flowIAT = new SummaryStatistics();
		this.forwardIAT = new SummaryStatistics();
		this.backwardIAT = new SummaryStatistics();
		this.flowActive = new SummaryStatistics();
		this.flowIdle = new SummaryStatistics();
		this.flowLengthStats = new SummaryStatistics();
		this.fwdPktStats = new SummaryStatistics();
		this.bwdPktStats =  new SummaryStatistics();
		this.flagCounts = new HashMap<String, MutableInt>();
		initFlags();
		this.forwardBytes = 0L;
		this.backwardBytes = 0L;	
		this.startActiveTime = 0L;
		this.endActiveTime = 0L;
		this.src = null;
		this.dst = null;
		this.fPSH_cnt=0;
		this.bPSH_cnt=0;
		this.fURG_cnt=0;
		this.bURG_cnt=0;
		this.fHeaderBytes=0L;
		this.bHeaderBytes=0L;

	}

	/**
	 * Set the first packet of this flow at the given packet
	 * Set the flow stat_time, active_time using the packet
	 * Update attributes according to the add of this packet
	 * @param packet to add to the flow as first packet
	 */
	public void firstPacket(BasicPacketInfo packet){
		updateFlowBulk(packet);
		detectUpdateSubflows(packet);
		checkFlags(packet);
		/* update flow time info */
		this.flowStartTime = packet.getTimeStamp();
		this.flowLastSeen = packet.getTimeStamp();
		this.startActiveTime = packet.getTimeStamp();
		this.endActiveTime = packet.getTimeStamp();
		this.flowLengthStats.addValue((double)packet.getPayloadBytes());

		/* set source info */
		if(this.src==null){
			this.src = packet.getSrc();
			this.srcPort = packet.getSrcPort();
		}
		/* set destination info */
		if(this.dst==null){
			this.dst = packet.getDst();
			this.dstPort = packet.getDstPort();
		}
		/* if the packet source == flow source then we are in the forward direction */
		if(Arrays.equals(this.src, packet.getSrc())){ //
			this.min_seg_size_forward = packet.getHeaderBytes();
			Init_Win_bytes_forward = packet.getTCPWindow();
			this.flowLengthStats.addValue((double)packet.getPayloadBytes());
			this.fwdPktStats.addValue((double)packet.getPayloadBytes());
			this.fHeaderBytes = packet.getHeaderBytes();
			this.forwardLastSeen = packet.getTimeStamp();
			this.forwardBytes+=packet.getPayloadBytes();
			this.forward.add(packet);
			if(packet.hasFlagPSH()){
				this.fPSH_cnt++;
			}
			if(packet.hasFlagURG()){
				this.fURG_cnt++;
			}
		/* if the packet source != flow source then we are in backward direction */
		}else{
			Init_Win_bytes_backward = packet.getTCPWindow();
			this.flowLengthStats.addValue((double)packet.getPayloadBytes());
			this.bwdPktStats.addValue((double)packet.getPayloadBytes());
			this.bHeaderBytes = packet.getHeaderBytes();
			this.backwardLastSeen = packet.getTimeStamp();
			this.backwardBytes+=packet.getPayloadBytes();
			this.backward.add(packet);
			if(packet.hasFlagPSH()){
				this.bPSH_cnt++;
			}
			if(packet.hasFlagURG()){
				this.bURG_cnt++;
			}
		}
		this.protocol = packet.getProtocol();
		this.flowId = packet.getFlowId();		
	}
    /**
	 * add one packet( as BasicPacketInfo ) to the flow
	 * @see BasicPacketInfo
	 * @param packet info about the packet added to the flow
	 */
    public void addPacket(BasicPacketInfo packet){
		updateFlowBulk(packet);
		detectUpdateSubflows(packet);
		checkFlags(packet);
    	long currentTimestamp = packet.getTimeStamp();
    	if(isBidirectional){
			this.flowLengthStats.addValue((double)packet.getPayloadBytes());
			/* if the packet source == flow source then we are in the forward direction */
			if(Arrays.equals(this.src, packet.getSrc())){
				if(packet.getPayloadBytes() >=1){
					this.Act_data_pkt_forward++;
				}
				this.fwdPktStats.addValue((double)packet.getPayloadBytes());
				this.fHeaderBytes +=packet.getHeaderBytes();
    			this.forward.add(packet);   
    			this.forwardBytes+=packet.getPayloadBytes();
    			if (this.forward.size()>1)
    				this.forwardIAT.addValue(currentTimestamp -this.forwardLastSeen);
    			this.forwardLastSeen = currentTimestamp;
				this.min_seg_size_forward = Math.min(packet.getHeaderBytes(),this.min_seg_size_forward);
			}/* if the packet source != flow source then we are in backward direction */
			else{
				this.bwdPktStats.addValue((double)packet.getPayloadBytes());
				Init_Win_bytes_backward = packet.getTCPWindow();
				this.bHeaderBytes+=packet.getHeaderBytes();
    			this.backward.add(packet);
    			this.backwardBytes+=packet.getPayloadBytes();
    			if (this.backward.size()>1)
    				this.backwardIAT.addValue(currentTimestamp-this.backwardLastSeen);
    			this.backwardLastSeen = currentTimestamp;
    		}
    	}
    	/* the packet is add to the flow and the flow is not bidirectional so it's a forward packet */
		else{
			if(packet.getPayloadBytes() >=1) {
				this.Act_data_pkt_forward++;
			}
			this.fwdPktStats.addValue((double)packet.getPayloadBytes());
			this.flowLengthStats.addValue((double)packet.getPayloadBytes());
			this.fHeaderBytes +=packet.getHeaderBytes();
    		this.forward.add(packet);    		
    		this.forwardBytes+=packet.getPayloadBytes();
    		this.forwardIAT.addValue(currentTimestamp-this.forwardLastSeen);
    		this.forwardLastSeen = currentTimestamp;
			this.min_seg_size_forward = Math.min(packet.getHeaderBytes(),this.min_seg_size_forward);
    	}
    	this.flowIAT.addValue(packet.getTimeStamp()-this.flowLastSeen);
    	this.flowLastSeen = packet.getTimeStamp();
    }

	/**
	 * Get the number of packets per Second in forward direction
	 * using the size of the forward list divided by the flow duration
	 * with duration is flowLastSeen - flowStartTime
	 * @see #forward
	 * @see #flowLastSeen
	 * @see #flowStartTime
	 * @return the number of packet per Second in forward direction
	 */
	public double getfPktsPerSecond(){
		long duration = this.flowLastSeen - this.flowStartTime;
		if(duration > 0){
			return (this.forward.size()/((double)duration/1000000L));
		}
		else
			return 0;
	}

	/**
	 * Get the number of packet per Second in backward direction
	 * using the size of the forward list divided by the flow duration
	 * with duration is flowLastSeen - flowStartTime
	 * @see #forward
	 * @see #flowLastSeen
	 * @see #flowStartTime
	 * @return the number of packet per Second in backward direction
	 */
	public double getbPktsPerSecond(){
		long duration = this.flowLastSeen - this.flowStartTime;
		if(duration > 0){
			return (this.backward.size()/((double)duration/1000000L));
		}
		else
			return 0;
	}

	/**
	 * Get the ratio of the number of backward direction packet / number of forward direction packet
	 * using the list of backward packets and the list of forward packet
	 * @see #forward
	 * @see #backward
	 * @return the ratio backward.size / forward.size
	 */
	public double getDownUpRatio(){
		if(this.forward.size() > 0){
			return (double)(this.backward.size()/this.forward.size());
		}
		return 0;
	}

	/**
	 * Get the mean of packetSize
	 * by taking the sum of all packet length divided by the number of packet
	 * @see #flowLengthStats
	 * @see #packetCount()
	 * @return the mean(packetSize)
	 */
	public double getAvgPacketSize(){
		if(this.packetCount() > 0){
			return (this.flowLengthStats.getSum()/this.packetCount());
		}
		return 0;
	}
	/**
	 * Get the mean of packetSize in forward direction
	 * by taking the sum of all packet length in forward direction divided by the number of packet in forward direction
	 * @see #fwdPktStats
	 * @see #forward
	 * @return the mean(packetSize in forward direction)
	 */
	public double fAvgSegmentSize(){
		if (this.forward.size()!=0)
			return (this.fwdPktStats.getSum() / (double)this.forward.size());
		return 0;
	}
	/**
	 * Get the mean of packetSize in backward direction
	 * by taking the sum of all packet length in backward direction divided by the number of packet in backward direction
	 * @see #bwdPktStats
	 * @see #backward
	 * @return the mean(packetSize in backward direction)
	 */
	public double bAvgSegmentSize(){
		if (this.backward.size()!=0)
			return (this.bwdPktStats.getSum() / (double)this.backward.size());
		return 0;
	}

	/**
	 * Initialise flagCounts with all accessible flag : FIN,SYN,RST,PSH,ACK,URG,CWR,ECE
	 * @see #flagCounts
	 **/
    public void initFlags(){
		flagCounts.put("FIN", new MutableInt());
		flagCounts.put("SYN", new MutableInt());
		flagCounts.put("RST", new MutableInt());
		flagCounts.put("PSH", new MutableInt());
		flagCounts.put("ACK", new MutableInt());
		flagCounts.put("URG", new MutableInt());
		flagCounts.put("CWR", new MutableInt());
		flagCounts.put("ECE", new MutableInt());
	}

	/**
	 * Get the flag of the given packet and increment the counts of this flag in flagCounts
	 * @see #flagCounts
	 * @param packet packet which flag is to be checked 
	 */
	public void checkFlags(BasicPacketInfo packet){
		if(packet.hasFlagFIN()){
			//MutableInt count1 = flagCounts.get("FIN");
			//count1.increment();
			flagCounts.get("FIN").increment();
		}
		if(packet.hasFlagSYN()){
			//MutableInt count2 = flagCounts.get("SYN");
			//count2.increment();
			flagCounts.get("SYN").increment();
		}
		if(packet.hasFlagRST()){
			//MutableInt count3 = flagCounts.get("RST");
			//count3.increment();
			flagCounts.get("RST").increment();
		}
		if(packet.hasFlagPSH()){
			//MutableInt count4 = flagCounts.get("PSH");
			//count4.increment();
			flagCounts.get("PSH").increment();
		}
		if(packet.hasFlagACK()){
			//MutableInt count5 = flagCounts.get("ACK");
			//count5.increment();
			flagCounts.get("ACK").increment();
		}
		if(packet.hasFlagURG()){
			//MutableInt count6 = flagCounts.get("URG");
			//count6.increment();
			flagCounts.get("URG").increment();
		}
		if(packet.hasFlagCWR()){
			//MutableInt count7 = flagCounts.get("CWR");
			//count7.increment();
			flagCounts.get("CWR").increment();
		}
		if(packet.hasFlagECE()){
			//MutableInt count8 = flagCounts.get("ECE");
			//count8.increment();
			flagCounts.get("ECE").increment();
		}
	}

	/**
	 * Get the mean length in byte of forward direction subflows
	 * @see #forwardBytes
	 * @see #sfCount
	 * @return forwardBytes/sfCount
	 */
	public long getSflow_fbytes(){
		if(sfCount <= 0) return 0;
		return this.forwardBytes/sfCount;
	}
	/**
	 * Get the mean number of packet in forward direction subflows
	 * @see #forward
	 * @see #sfCount
	 * @return forward.size()/sfCount
	 */
	public long getSflow_fpackets(){
		if(sfCount <= 0) return 0;
		return this.forward.size()/sfCount;
	}
	/**
	 * Get the mean length in byte of backward direction subflows
	 * @see #backwardBytes
	 * @see #sfCount
	 * @return backwardBytes/sfCount
	 */
	public long getSflow_bbytes(){
		if(sfCount <= 0) return 0;
		return this.backwardBytes/sfCount;
	}
	/**
	 * Get the mean number of packet in backward direction subflows
	 * @see #backward
	 * @see #sfCount
	 * @return backward.size()/sfCount
	 */
	public long getSflow_bpackets(){
		if(sfCount <= 0) return 0;
		return this.backward.size()/sfCount;
	}

	private long sfLastPacketTS=-1;
	private int sfCount=0;
	private long sfAcHelper=-1;

	/**
	 * if the packet is the first for detection
	 * or if the packet active time is 1s or more than the last packets seen
	 * add 1 to sfCount
	 * update the active time
	 * set SfAcHelper at the time of the packet
	 * @see #sfCount
	 * @see #updateActiveIdleTime(long, long)
	 * @see #sfAcHelper
	 * @see BasicPacketInfo#getTimeStamp()
	 * @param packet packet for the Subflow update
	 */
	void detectUpdateSubflows( BasicPacketInfo packet ){
		if(sfLastPacketTS == -1){
			sfLastPacketTS = packet.getTimeStamp();
			sfAcHelper   = packet.getTimeStamp();
		}
		//System.out.print(" - "+(packet.timeStamp - sfLastPacketTS));
		if( (packet.getTimeStamp() - (sfLastPacketTS)/(double)1000000)   > 1.0 ){
			sfCount ++ ;
			long lastSFduration = packet.getTimeStamp() - sfAcHelper;
			updateActiveIdleTime(packet.getTimeStamp() - sfLastPacketTS, 5000000L);
			sfAcHelper = packet.getTimeStamp();
		}

		sfLastPacketTS = packet.getTimeStamp() ;
	}

	////////////////////////////// BULK PART ////////////////////////////////////////
	private long fbulkDuration=0;
	private long fbulkPacketCount=0;
	private long fbulkSizeTotal=0;
	private long fbulkStateCount=0;
	private long fbulkPacketCountHelper=0;
	private long fbulkStartHelper=0;
	private long fbulkSizeHelper=0;
	private long flastBulkTS=0;
	private long bbulkDuration=0;
	private long bbulkPacketCount=0;
	private long bbulkSizeTotal=0;
	private long bbulkStateCount=0;
	private long bbulkPacketCountHelper=0;
	private long bbulkStartHelper=0;
	private long bbulkSizeHelper=0;
	private long blastBulkTS=0;

	/**
	 * Check if the packet is in the forward direction or backward direction and update the bulk
	 * @see #updateForwardBulk(BasicPacketInfo packet , long blastBulkTS)
	 * @see #updateBackwardBulk(BasicPacketInfo packet, long flastBulkTS)
	 * @param packet packet for update
	 */
	public void updateFlowBulk (BasicPacketInfo packet){
		if(this.src == packet.getSrc()){
			updateForwardBulk(packet,blastBulkTS);
		}else {
			updateBackwardBulk(packet,flastBulkTS);
		}

	}

	/**
	 * Update the bulk for the forward direction :
	 *
	 * @param packet packet for update
	 *
	 * @param tsOflastBulkInOther time stamp of flast Bulk In Other
	 */
	public void updateForwardBulk(BasicPacketInfo packet, long tsOflastBulkInOther){

		long size=packet.getPayloadBytes();
		if (tsOflastBulkInOther > fbulkStartHelper) fbulkStartHelper = 0;
		if (size <= 0) return ;

		packet.getPayloadPacket();

		if (fbulkStartHelper == 0){
			fbulkStartHelper = packet.getTimeStamp();
			fbulkPacketCountHelper = 1;
			fbulkSizeHelper = size ;
			flastBulkTS = packet.getTimeStamp();
		} //possible bulk
		else{
			// Too much idle time?
			if (((packet.getTimeStamp() - flastBulkTS)/(double)1000000) > 1.0){
				fbulkStartHelper = packet.getTimeStamp();
				flastBulkTS = packet.getTimeStamp();
				fbulkPacketCountHelper = 1;
				fbulkSizeHelper = size;
			}// Add to bulk
			else{
				fbulkPacketCountHelper += 1;
				fbulkSizeHelper        += size ;
				//New bulk
				if (fbulkPacketCountHelper == 4){
					fbulkStateCount  += 1;
					fbulkPacketCount += fbulkPacketCountHelper;
					fbulkSizeTotal   += fbulkSizeHelper;
					fbulkDuration    += packet.getTimeStamp() - fbulkStartHelper;
				} //Continuation of existing bulk
				else if (fbulkPacketCountHelper > 4){
					fbulkPacketCount += 1;
					fbulkSizeTotal   += size;
					fbulkDuration    += packet.getTimeStamp() - flastBulkTS;
				}
				flastBulkTS = packet.getTimeStamp();
			}
		}
	}
	/**
	 * Update the bulk for the Backward direction :
	 *
	 * @param packet packet for update
	 * @param tsOflastBulkInOther time stamp of flast Bulk In Other
	 */
	public void updateBackwardBulk(BasicPacketInfo packet , long tsOflastBulkInOther){
		/*bAvgBytesPerBulk =0;
		bbulkSizeTotal=0;
		bbulkStateCount=0;*/
		long size=packet.getPayloadBytes();
		if (tsOflastBulkInOther > bbulkStartHelper) bbulkStartHelper = 0;
		if ( size<= 0) return ;

		packet.getPayloadPacket();

		if ( bbulkStartHelper == 0 ){
			bbulkStartHelper = packet.getTimeStamp();
			bbulkPacketCountHelper = 1;
			bbulkSizeHelper = size ;
			blastBulkTS = packet.getTimeStamp();
		} //possible bulk
		else{
			// Too much idle time?
			if (((packet.getTimeStamp() - blastBulkTS)/(double)1000000) > 1.0){
				bbulkStartHelper = packet.getTimeStamp();
				blastBulkTS = packet.getTimeStamp();
				bbulkPacketCountHelper = 1;
				bbulkSizeHelper = size;
			}// Add to bulk
			else{
				bbulkPacketCountHelper += 1;
				bbulkSizeHelper += size ;
				//New bulk
				if (bbulkPacketCountHelper == 4){
					bbulkStateCount  += 1;
					bbulkPacketCount += bbulkPacketCountHelper;
					bbulkSizeTotal   += bbulkSizeHelper;
					bbulkDuration    += packet.getTimeStamp() - bbulkStartHelper;
				} //Continuation of existing bulk
				else if (bbulkPacketCountHelper > 4){
					bbulkPacketCount += 1;
					bbulkSizeTotal   += size;
					bbulkDuration    += packet.getTimeStamp() - blastBulkTS;
				}
				blastBulkTS = packet.getTimeStamp();
			}
		}

	}

	public  long fbulkStateCount() {
		return fbulkStateCount;
	}

	public  long fbulkSizeTotal() {
		return fbulkSizeTotal;
	}

	public long fbulkPacketCount() {
		return fbulkPacketCount;
	}

	public long fbulkDuration() {
		return fbulkDuration;
	}
	public double fbulkDurationInSecond() {
		return fbulkDuration/(double)1000000;
	}

	//Client average bytes per bulk
	/**
	 * Client average bytes per bulk
	 * Return the average number of bytes per bulk
	 * @see #fbulkSizeTotal()
	 * @see #fbulkStateCount()
	 * @return fbulkSizeTotal()/fbulkStateCount()
	 */
	public long fAvgBytesPerBulk(){
		if (this.fbulkStateCount() != 0 )
			return (this.fbulkSizeTotal() / this.fbulkStateCount());
		return 0;
	}

	//Client average packets per bulk
	/**
	 * Client average packets per bulk
	 * Return the average number of packets per bulk
	 * @see #fbulkPacketCount()
	 * @see #fbulkStateCount()
	 * @return fbulkPacketCount()/fbulkStateCount()
	 */
	public long fAvgPacketsPerBulk(){
		if (this.fbulkStateCount() != 0 )
			return (this.fbulkPacketCount() / this.fbulkStateCount());
		return 0;
	}


	//Client average bulk rate

	/**
	 * Client average bulk rate
	 * get the ratio of the number of packet in forward direction / duration of bulk in forward direction
	 * @see #fbulkSizeTotal()
	 * @see #fbulkDurationInSecond()
	 * @return (fbulkSizeTotal() / fbulkDurationInSecond())
	 */
	public long fAvgBulkRate(){
		if (this.fbulkDuration() != 0 )
			return (long)(this.fbulkSizeTotal() / this.fbulkDurationInSecond());
		return 0;
	}


	//new features server
	public long bbulkPacketCount() {
		return bbulkPacketCount;
	}

	public long bbulkStateCount() {
		return bbulkStateCount;
	}

	public long bbulkSizeTotal() {
		return bbulkSizeTotal;
	}

	public long bbulkDuration() {
		return bbulkDuration;
	}
	public double bbulkDurationInSecond() {
		return bbulkDuration/(double)1000000;
	}

	//Server average bytes per bulk
	public long bAvgBytesPerBulk(){
		if(this.bbulkStateCount() != 0)
			return (this.bbulkSizeTotal() /  this.bbulkStateCount());
		return 0;
	}

	//Server average packets per bulk
	public long bAvgPacketsPerBulk(){
		if(this.bbulkStateCount() != 0 )
			return (this.bbulkPacketCount() /  this.bbulkStateCount());
		return 0;
	}
	//Server average bulk rate
	public long bAvgBulkRate(){
		if(this.bbulkDuration() != 0)
			return (long)(this.bbulkSizeTotal() / this.bbulkDurationInSecond());
		return 0;
	}

	//////////////////////////// END OF BULK PART ///////////////////////////////////////////////

	/**
	 * Update the active time :
	 * Check if do not update the active time to soon, using the treshold
	 * and if we do not : update flowactive and flow IDLE ; set start active time and end active time at curent time
	 * @see #flowActive
	 * @see #flowIdle
	 * @param currentTime time at which active time will be updated
	 * @param threshold minimum time between the last update to change flowActive and flowIdle
	 */
    public void updateActiveIdleTime(long currentTime, long threshold){
    	if ((currentTime - this.endActiveTime) > threshold){
    		if((this.endActiveTime - this.startActiveTime) > 0){
	      		this.flowActive.addValue(this.endActiveTime - this.startActiveTime);	      		
    		}
    		this.flowIdle.addValue(currentTime - this.endActiveTime);
    		this.startActiveTime = currentTime;
    		this.endActiveTime = currentTime;
    	} else{
    		this.endActiveTime = currentTime;
    	}
    }

	/**
	 * not used
	 * @param currentTime not used
	 * @param threshold not used
	 * @param flowTimeOut use to check a timout
	 * @param isFlagEnd check if the flow is ended
	 */
    public void endActiveIdleTime(long currentTime, long threshold, long flowTimeOut, boolean isFlagEnd){
		
    	if((this.endActiveTime - this.startActiveTime) > 0){
      		this.flowActive.addValue(this.endActiveTime - this.startActiveTime);	      		
		}
    	
    	if (!isFlagEnd && ((flowTimeOut - (this.endActiveTime-this.flowStartTime))>0)){
    		this.flowIdle.addValue(flowTimeOut - (this.endActiveTime-this.flowStartTime));
    	}
    }

	/**
	 * @deprecated
	 * Try to dump the flow features
	 * @return flowfeatures
	 */
    public String dumpFlowBasedFeatures(){
    	String dump = "";
		dump+=this.flowId+",";
    	dump+=FormatUtils.ip(src)+",";
    	dump+=getSrcPort()+",";
    	dump+=FormatUtils.ip(dst)+",";    			
    	dump+=getDstPort()+",";
    	dump+=getProtocol()+",";
		//dump+=this.flowStartTime+",";
    	dump+=DateFormatter.parseDateFromLong(this.flowStartTime/1000L, "dd/MM/yyyy hh:mm:ss")+",";
    	long flowDuration = this.flowLastSeen - this.flowStartTime; 
    	dump+=flowDuration+",";
		dump+=this.fwdPktStats.getN()+",";
		dump+=this.bwdPktStats.getN()+",";
		dump+=this.fwdPktStats.getSum()+",";
		dump+=this.bwdPktStats.getSum()+",";
		if(fwdPktStats.getN() > 0L) {
			dump += this.fwdPktStats.getMax() + ",";
			dump += this.fwdPktStats.getMin() + ",";
			dump += this.fwdPktStats.getMean() + ",";
			dump += this.fwdPktStats.getStandardDeviation() + ",";
		}else{
			dump+="0,0,0,0,";
		}
		if(bwdPktStats.getN() > 0L) {
			dump += this.bwdPktStats.getMax() + ",";
			dump += this.bwdPktStats.getMin() + ",";
			dump += this.bwdPktStats.getMean() + ",";
			dump += this.bwdPktStats.getStandardDeviation() + ",";
		}else{
			dump+="0,0,0,0,";
		}
    	// flow duration is in microseconds, therefore packets per seconds = packets / (duration/1000000)
    	dump+=((double)(this.forwardBytes+this.backwardBytes))/((double)flowDuration/1000000L)+",";    			
    	dump+=((double)packetCount())/((double)flowDuration/1000000L)+",";
    	dump+=this.flowIAT.getMean()+",";
    	dump+=this.flowIAT.getStandardDeviation()+",";
    	dump+=this.flowIAT.getMax()+",";
    	dump+=this.flowIAT.getMin()+",";    	
    	if(this.forward.size()>1){
			dump+=this.forwardIAT.getSum()+",";
        	dump+=this.forwardIAT.getMean()+",";
        	dump+=this.forwardIAT.getStandardDeviation()+",";
        	dump+=this.forwardIAT.getMax()+",";
        	dump+=this.forwardIAT.getMin()+",";
    	}else{
    		dump+="0,0,0,0,0,";
    	}
    	if(this.backward.size()>1){
			dump+=this.backwardIAT.getSum()+",";
        	dump+=this.backwardIAT.getMean()+",";
        	dump+=this.backwardIAT.getStandardDeviation()+",";
        	dump+=this.backwardIAT.getMax()+",";
        	dump+=this.backwardIAT.getMin()+","; 
    	}else{
    		dump+="0,0,0,0,0,";
    	}

		dump+=this.fPSH_cnt+",";
		dump+=this.bPSH_cnt+",";
		dump+=this.fURG_cnt+",";
		dump+=this.bURG_cnt+",";

		dump+=this.fHeaderBytes+",";
		dump+=this.bHeaderBytes+",";
		dump+=getfPktsPerSecond()+",";
		dump+=getbPktsPerSecond()+",";

		if(this.forward.size() > 0 || this.backward.size() > 0){
			dump+=this.flowLengthStats.getMin()+",";
			dump+=this.flowLengthStats.getMax()+",";
			dump+=this.flowLengthStats.getMean()+",";
			dump+=this.flowLengthStats.getStandardDeviation()+",";
			dump+=flowLengthStats.getVariance()+",";
		}else{
			dump+="0,0,0,0,";
		}

		for(String key: flagCounts.keySet()){
			dump+=flagCounts.get(key).value+",";
		}

		dump+=getDownUpRatio()+",";
		dump+=getAvgPacketSize()+",";
		dump+=fAvgSegmentSize()+",";
		dump+=bAvgSegmentSize()+",";
		dump+=this.fHeaderBytes+",";  //this feature is duplicated



		dump+=fAvgBytesPerBulk()+",";
		dump+=fAvgPacketsPerBulk()+",";
		dump+=fAvgBulkRate()+",";
		dump+=fAvgBytesPerBulk()+",";
		dump+=bAvgPacketsPerBulk()+",";
		dump+=bAvgBulkRate()+",";

		dump+=getSflow_fpackets()+",";
		dump+=getSflow_fbytes()+",";
		dump+=getSflow_bpackets()+",";
		dump+=getSflow_bbytes()+",";

		dump+=this.Init_Win_bytes_forward+",";
		dump+=this.Init_Win_bytes_backward+",";
		dump+=this.Act_data_pkt_forward+",";
		dump+=this.min_seg_size_forward+",";

    	if(this.flowActive.getN()>0){
        	dump+=this.flowActive.getMean()+",";
        	dump+=this.flowActive.getStandardDeviation()+",";
        	dump+=this.flowActive.getMax()+",";
        	dump+=this.flowActive.getMin()+",";  
    	}else{
    		dump+="0,0,0,0,";
    	}    	
    	
    	if(this.flowIdle.getN()>0){
	    	dump+=this.flowIdle.getMean()+",";
	    	dump+=this.flowIdle.getStandardDeviation()+",";
	    	dump+=this.flowIdle.getMax()+",";
	    	dump+=this.flowIdle.getMin();    
    	}else{
    		dump+="0,0,0,0";
    	}
		dump+=","+ getLabel();

		/*if(FormatUtils.ip(src).equals("147.32.84.165") | FormatUtils.ip(dst).equals("147.32.84.165")){
			dump+=",BOTNET";
		}
		else{
			dump+=",BENIGN";
		} */
		/////////////////////////////////
    	return dump;
    }

	/**
	 * count the number of packets in the flow as the forward + backward size for bidirectional flow
	 * and only forward size for unidirectional flow
	 * @return the number of packets in the flow
	 */
	public int packetCount(){
    	if(isBidirectional){
    		return (this.forward.size() + this.backward.size()); 
    	}else{
    		return this.forward.size();    		
    	}
    }

	/**
	 * Give a copy of the src array
	 * @see #src
	 * @return byte[] that is a copy of the src array list
	 */
	public byte[] getSrc() {
		return Arrays.copyOf(src,src.length);
	}

	/**
	 * Give a copy of the dst array
	 * @see #dst
	 * @return byte[] that is a copy of the dst array list
	 */
	public byte[] getDst() {
		return Arrays.copyOf(dst,dst.length);
	}

	/**
	 * Getter for the source port of the flow
	 * @see #srcPort
	 * @return the flow source Port
	 */
	public int getSrcPort() {
		return srcPort;
	}

	/**
	 * Getter for the destination port of the flow
	 * @see #dstPort
	 * @return the flow destination port
	 */
	public int getDstPort() {
		return dstPort;
	}

	/**
	 * Getter for the protocol of the flow
	 * @see #protocol
	 * @return the flow protocol : 6 for TCP and 17 for UDP
	 */
	public int getProtocol() {
		return protocol;
	}

	/**
	 * Getter for flowStartTime (time when the flow starts)
	 * @see #flowStartTime
	 * @return flowStartTime
	 */
	public long getFlowStartTime() {
		return flowStartTime;
	}

	public String getLabel() {
		//the original is "|". I think it should be "||" need to check,
		/*if(FormatUtils.ip(src).equals("147.32.84.165") || FormatUtils.ip(dst).equals("147.32.84.165")){
			return "BOTNET";													
		}
		else{
			return "BENIGN";
		}*/
        return "NeedManualLabel";
    }

	/**
	 * Generate flow features:
	 * Generate 84 features using flow infomation and dump it as a string
	 * features ar separate using separator
	 * @see #separator
	 * @see FlowFeature
	 * @return all the 84 features separated by ","
	 */
    public String dumpFlowBasedFeaturesEx() {
    	StringBuilder dump = new StringBuilder();

    	//The number on the right of each line represent the feature calculated using the enumeration flowFeature

		dump.append(flowId).append(separator);                						//1
    	dump.append(FormatUtils.ip(src)).append(separator);   						//2
    	dump.append(getSrcPort()).append(separator);          						//3
    	dump.append(FormatUtils.ip(dst)).append(separator);  						//4
    	dump.append(getDstPort()).append(separator);          						//5
    	dump.append(getProtocol()).append(separator);         						//6 
    	
    	String starttime = DateFormatter.convertMilliseconds2String(flowStartTime/1000L, "dd/MM/yyyy hh:mm:ss a");
    	dump.append(starttime).append(separator);									//7
    	
    	long flowDuration = flowLastSeen - flowStartTime;
    	dump.append(flowDuration).append(separator);								//8
    	
    	dump.append(fwdPktStats.getN()).append(separator);							//9
    	dump.append(bwdPktStats.getN()).append(separator);							//10	
    	dump.append(fwdPktStats.getSum()).append(separator);						//11
    	dump.append(bwdPktStats.getSum()).append(separator);						//12
    	
    	if(fwdPktStats.getN() > 0L) {
    		dump.append(fwdPktStats.getMax()).append(separator);					//13
    		dump.append(fwdPktStats.getMin()).append(separator);					//14
    		dump.append(fwdPktStats.getMean()).append(separator);					//15
    		dump.append(fwdPktStats.getStandardDeviation()).append(separator);		//16
    	}else {
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    	}
    	
    	if(bwdPktStats.getN() > 0L) {
    		dump.append(bwdPktStats.getMax()).append(separator);					//17
    		dump.append(bwdPktStats.getMin()).append(separator);					//18
    		dump.append(bwdPktStats.getMean()).append(separator);					//19
    		dump.append(bwdPktStats.getStandardDeviation()).append(separator);		//20
		}else{
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
		}
    	dump.append(((double)(forwardBytes+backwardBytes))/((double)flowDuration/1000000L)).append(separator);//21
    	dump.append(((double)packetCount())/((double)flowDuration/1000000L)).append(separator);//22
    	dump.append(flowIAT.getMean()).append(separator);							//23
    	dump.append(flowIAT.getStandardDeviation()).append(separator);				//24
    	dump.append(flowIAT.getMax()).append(separator);							//25
    	dump.append(flowIAT.getMin()).append(separator);							//26
    	
    	if(this.forward.size()>1){
        	dump.append(forwardIAT.getSum()).append(separator);						//27
        	dump.append(forwardIAT.getMean()).append(separator);					//28
        	dump.append(forwardIAT.getStandardDeviation()).append(separator);		//29	
        	dump.append(forwardIAT.getMax()).append(separator);						//30
        	dump.append(forwardIAT.getMin()).append(separator);						//31
        	
    	}else{
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    	}
    	if(this.backward.size()>1){
        	dump.append(backwardIAT.getSum()).append(separator);					//32
        	dump.append(backwardIAT.getMean()).append(separator);					//33
        	dump.append(backwardIAT.getStandardDeviation()).append(separator);		//34	
        	dump.append(backwardIAT.getMax()).append(separator);					//35
        	dump.append(backwardIAT.getMin()).append(separator);					//36
    	}else{
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    	}
    	
		dump.append(fPSH_cnt).append(separator);									//37
		dump.append(bPSH_cnt).append(separator);									//38
		dump.append(fURG_cnt).append(separator);									//39
		dump.append(bURG_cnt).append(separator);									//40

		dump.append(fHeaderBytes).append(separator);								//41
		dump.append(bHeaderBytes).append(separator);								//42
		dump.append(getfPktsPerSecond()).append(separator);							//43
		dump.append(getbPktsPerSecond()).append(separator);							//44
		
		
		if(this.forward.size() > 0 || this.backward.size() > 0){
			dump.append(flowLengthStats.getMin()).append(separator);				//45
			dump.append(flowLengthStats.getMax()).append(separator);				//46
			dump.append(flowLengthStats.getMean()).append(separator);				//47
			dump.append(flowLengthStats.getStandardDeviation()).append(separator);	//48
			dump.append(flowLengthStats.getVariance()).append(separator);			//49
		}else{//seem to less one
			dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
		}
		
		/*for(MutableInt v:flagCounts.values()) {
			dump.append(v).append(separator);
		}
		for(String key: flagCounts.keySet()){
			dump.append(flagCounts.get(key).value).append(separator);				//50,51,52,53,54,55,56,57
		} */
		dump.append(flagCounts.get("FIN").value).append(separator);                 //50
		dump.append(flagCounts.get("SYN").value).append(separator);                 //51
		dump.append(flagCounts.get("RST").value).append(separator);                  //52
		dump.append(flagCounts.get("PSH").value).append(separator);                  //53
		dump.append(flagCounts.get("ACK").value).append(separator);                  //54
		dump.append(flagCounts.get("URG").value).append(separator);                  //55
		dump.append(flagCounts.get("CWR").value).append(separator);                  //56
		dump.append(flagCounts.get("ECE").value).append(separator);                  //57
		
		dump.append(getDownUpRatio()).append(separator);							//58
		dump.append(getAvgPacketSize()).append(separator);							//59
		dump.append(fAvgSegmentSize()).append(separator);							//60
		dump.append(bAvgSegmentSize()).append(separator);							//61
		//dump.append(fHeaderBytes).append(separator);								//62 dupicate with 41
		
		dump.append(fAvgBytesPerBulk()).append(separator);							//63	
		dump.append(fAvgPacketsPerBulk()).append(separator);						//64
		dump.append(fAvgBulkRate()).append(separator);								//65
		dump.append(fAvgBytesPerBulk()).append(separator);							//66
		dump.append(bAvgPacketsPerBulk()).append(separator);						//67
		dump.append(bAvgBulkRate()).append(separator);								//68
    	
		dump.append(getSflow_fpackets()).append(separator);							//69
		dump.append(getSflow_fbytes()).append(separator);							//70
		dump.append(getSflow_bpackets()).append(separator);							//71
		dump.append(getSflow_bbytes()).append(separator);							//72
			
    	dump.append(Init_Win_bytes_forward).append(separator);						//73
    	dump.append(Init_Win_bytes_backward).append(separator);						//74
    	dump.append(Act_data_pkt_forward).append(separator);						//75
    	dump.append(min_seg_size_forward).append(separator);						//76
    	
    	
    	if(this.flowActive.getN()>0){
        	dump.append(flowActive.getMean()).append(separator);					//77
        	dump.append(flowActive.getStandardDeviation()).append(separator);		//78
        	dump.append(flowActive.getMax()).append(separator);						//79
        	dump.append(flowActive.getMin()).append(separator);						//80
    	}else{
			dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    	}    	
    	
    	if(this.flowIdle.getN()>0){
	    	dump.append(flowIdle.getMean()).append(separator);						//81
	    	dump.append(flowIdle.getStandardDeviation()).append(separator);			//82
	    	dump.append(flowIdle.getMax()).append(separator);						//83
	    	dump.append(flowIdle.getMin()).append(separator);						//84	
    	}else{
			dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    		dump.append(0).append(separator);
    	}

        dump.append(getLabel());
    	return dump.toString();
    }
}

/**
 * Used to count
 */
class MutableInt {
	/**
	 * Value that will be incremented to count, that why it's 0
	 */
	int value = 0; // note that we start at 1 since we're counting

	/**
	 * add 1 to the value
	 */
	public void increment () { ++value;  }

	/**
	 * Getter for the value
	 * @see #value
	 * @return value
	 */
	public int  get ()       { return value; }
	
}

package cic.cs.unb.ca.jnetpcap;

/**
 *  Generate an id
 */
public class IdGenerator {
	
	private long id = 0L;

	/**
	 * Constructor
	 * @param id id of the object
	 */
	public IdGenerator(long id) {
		super();
		this.id = id;
	}

	public IdGenerator() {
		super();
		this.id = 0L;
	}

	/**
	 * Alow to get the next id
	  The function is synchronized so every id return should be unique
	 * @return an id that can be used as primary key
	 */
	public synchronized long nextId(){
		this.id++;
		return this.id;
	}

}

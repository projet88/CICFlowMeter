package cic.cs.unb.ca.jnetpcap;

import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class Utils {
    protected static final Logger logger = LoggerFactory.getLogger(Utils.class);
    public static final String FILE_SEP = System.getProperty("file.separator");
    public static final String LINE_SEP = System.lineSeparator();
    /**
     * MediaType of pcap, used to test the type of files
     */
    private final static String PCAP = "application/vnd.tcpdump.pcap";
    public static final String FLOW_SUFFIX = "_Flow.csv";

    /**
     * compares the given String with PCAP 
     *
     * @param contentType string to be tested equal to application/vnd.tcpdump.pcap
     * @return true if the string is equal to application/vnd.tcpdump.pcap else false
     * @see #PCAP
     */
    private static boolean isPcapFile(String contentType) {
        return PCAP.equalsIgnoreCase(contentType);
    }

    /**
     * Tests if a file is a pcap file
     * gets the meta data of the file using Tika
     *
     * @param file file to be tested
     * @return true if the file is a pcap file
     * @see Tika
     * and check with
     * @see #isPcapFile(String filemetadata)
     */
    public static boolean isPcapFile(File file) {
        if (file == null) {
            return false;
        }
        try {
            //Files.probeContentType returns null on Windows
            /*Path filePath = Paths.get(file.getPath());
            contentType = Files.probeContentType(filePath);*/
            return isPcapFile(new Tika().detect(file));
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }
        return false;
    }

    /**
     * not used
     *
     * @param stream to be tested
     * @return true if the stream corresponds to a pcap file
     */
    public static boolean isPcapFile(InputStream stream) {
        if (stream == null) {
            return false;
        }
        try {
            return isPcapFile(new Tika().detect(stream));
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }
        return false;
    }

    /**
     * counts the number of lines in the file pointed by fileName
     * @param fileName path to the file
     * @return the number of lines in the file
     */
    public static long countLines(String fileName) {
        File file = new File(fileName);
        int linenumber = 0;
        FileReader fr; //allow to read the file
        LineNumberReader lnr = null; // allows to get the number of lines 
        try { // Count of the number of lines
            fr = new FileReader(file);
            lnr = new LineNumberReader(fr);
            while (lnr.readLine() != null) {
                linenumber++;
            }
        } catch (IOException e) {
            logger.debug(e.getMessage());
        } finally { // even if an exception occurred
            if (lnr != null) { // we close the LineNumberReader if it is not null
                try {
                    lnr.close();
                } catch (IOException e) {
                    logger.debug(e.getMessage());
                }
            }
        }
        return linenumber;
    }
}

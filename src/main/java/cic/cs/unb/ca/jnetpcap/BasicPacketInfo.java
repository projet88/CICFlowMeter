package cic.cs.unb.ca.jnetpcap;

import java.util.Arrays;
import org.jnetpcap.packet.format.FormatUtils;

/**
 * contains all informations about a packet
 */
public class BasicPacketInfo {
	
/*  Basic infos to generate flows from packets  	*/
	/**
	 * identifier of the BasicPacketInfo
	 */
    private    long id;
	/**
	 * source ip of the packet
	 */
	private    byte[] src;
	/**
	 * destination ip of the packet
	 */
    private    byte[] dst;
    /**
	 * source port of the packet
     */
    private    int srcPort;
	/**
	 * destination port of the packet
	 */
	private    int dstPort;
	/**
	 * transfer protocol
	 */
    private    int protocol;
    /**
	 * packet timeStamp
     */
    private    long   timeStamp;
    /**
	 * packet payload
     */
    private    long   payloadBytes;
	/**
	 * the packet is on a packet flow
	 * The flow id the identifier of this flow
	 */
	private    String  flowId = null;
/* ******************************************** */
	/**
	 * if the packet have the FIN flag
	 */
	private    boolean flagFIN = false;
	/**
	 * if the packet have the PSH flag
	 */
	private    boolean flagPSH = false;
	/**
	 * if the packet have the URG flag
	 */
	private    boolean flagURG = false;
	/**
	 * if the packet have the ECE flag
	 */
	private    boolean flagECE = false;
	/**
	 * if the packet have the SYN flag
	 */
	private    boolean flagSYN = false;
	/**
	 * if the packet have the ACK flag
	 */
	private    boolean flagACK = false;
	/**
	 * if the packet have the CWR flag
	 */
	private    boolean flagCWR = false;
	/**
	 * if the packet have the RST flag
	 */
	private    boolean flagRST = false;

	private	   int TCPWindow=0;

	private	   long headerBytes;

	private int payloadPacket=0;

	/**
	 * never used
	 * @param src source ip
	 * @param dst destination ip
	 * @param srcPort source port
	 * @param dstPort destination port
	 * @param protocol protocol
	 * @param timeStamp timeStamp of the packet
	 * @param generator the generator for the id of the packet
	 */
	public BasicPacketInfo(byte[] src, byte[] dst, int srcPort, int dstPort,
			int protocol, long timeStamp, IdGenerator generator) {
		super();
		this.id = generator.nextId();
		this.src = src;
		this.dst = dst;
		this.srcPort = srcPort;
		this.dstPort = dstPort;
		this.protocol = protocol;
		this.timeStamp = timeStamp;
		generateFlowId();
	}

	/**
	 * Constructor creates a new BasicPacketInfo and set it id with the given generator
	 * @param generator the generator that will generate the id of the object
	 */
    public BasicPacketInfo(IdGenerator generator) {
		super();
		this.id = generator.nextId();
	}

	/**
	 * Allow to generate an ID using the source ip and the destination ip
	 * the id will be the same if the packet travels in forward src -> dest or backward dest -> src direction
	 * the flow id is set as the generated id
	 * @return the flow id
	 */
	public String generateFlowId(){
    	boolean forward = true; // the flow is considered forward as default
		/*arbitrary way to set the direction using source ip  and destination ip*/
    	for(int i=0; i<this.src.length;i++){
    		if(((Byte)(this.src[i])).intValue() != ((Byte)(this.dst[i])).intValue()){ // check equality
    			/*will alow that if we have ip1 -> ip2 that is forward ip2 -> ip1 will be backward with the same id */
    			if(((Byte)(this.src[i])).intValue() >((Byte)(this.dst[i])).intValue()){
    				forward = false;
    			}
    			i=this.src.length;
    		}
    	}
        if(forward){
            this.flowId = this.getSourceIP() + "-" + this.getDestinationIP() + "-" + this.srcPort  + "-" + this.dstPort  + "-" + this.protocol;
        }else{
            this.flowId = this.getDestinationIP() + "-" + this.getSourceIP() + "-" + this.dstPort  + "-" + this.srcPort  + "-" + this.protocol;
        }
        return this.flowId;
	}

	/**
	 * generate the flow id considering it's a forward flow
	 * @return the flow id
	 */
 	public String fwdFlowId() {  
		this.flowId = this.getSourceIP() + "-" + this.getDestinationIP() + "-" + this.srcPort  + "-" + this.dstPort  + "-" + this.protocol;
		return this.flowId;
	}

	/**
	 * generate the flow id considering it's a backward flow
	 * @return the flow id
	 */
	public String bwdFlowId() {  
		this.flowId = this.getDestinationIP() + "-" + this.getSourceIP() + "-" + this.dstPort  + "-" + this.srcPort  + "-" + this.protocol;
		return this.flowId;
	}


	/**
	 * increment payloadPacket by one and return it
	 * @see #payloadPacket
	 * @return payloadPacket+=1
	 */
	public int getPayloadPacket() {
		return payloadPacket+=1;
	}

	/**
	 * Get the source ip of the packet using FormatUtis
	 * @return the source ip as a String
	 */
	public String getSourceIP(){
    	return FormatUtils.ip(this.src);
    }

	/**
	 * Get the destination ip of the packet using FormatUtis
	 * @return the destination ip as a String
	 */
    public String getDestinationIP(){
    	return FormatUtils.ip(this.dst);
    }
    
   	public byte[] getSrc() {
		return Arrays.copyOf(src,src.length);
	}

	public void setSrc(byte[] src) {
		this.src = src;
	}

	public byte[] getDst() {
		return Arrays.copyOf(dst,dst.length);
	}

	public void setDst(byte[] dst) {
		this.dst = dst;
	}

	public int getSrcPort() {
		return srcPort;
	}

	public void setSrcPort(int srcPort) {
		this.srcPort = srcPort;
	}

	public int getDstPort() {
		return dstPort;
	}

	public void setDstPort(int dstPort) {
		this.dstPort = dstPort;
	}

	public int getProtocol() {
		return protocol;
	}

	public void setProtocol(int protocol) {
		this.protocol = protocol;
	}

	/**
	 * Time when the packet was send
 	 */
	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getFlowId() {
		return this.flowId!=null?this.flowId:generateFlowId();
	}

	public void setFlowId(String flowId) {		
		this.flowId = flowId;
	}

	public boolean isForwardPacket(byte[] sourceIP) {
		return Arrays.equals(sourceIP, this.src);
	}

	public long getPayloadBytes() {
		return payloadBytes;
	}

	public void setPayloadBytes(long payloadBytes) {
		this.payloadBytes = payloadBytes;
	}

	public long getHeaderBytes() {
		return headerBytes;
	}

	public void setHeaderBytes(long headerBytes) {
		this.headerBytes = headerBytes;
	}

	public boolean hasFlagFIN() {
		return flagFIN;
	}

	public void setFlagFIN(boolean flagFIN) {
		this.flagFIN = flagFIN;
	}

	public boolean hasFlagPSH() {
		return flagPSH;
	}

	public void setFlagPSH(boolean flagPSH) {
		this.flagPSH = flagPSH;
	}

	public boolean hasFlagURG() {
		return flagURG;
	}

	public void setFlagURG(boolean flagURG) {
		this.flagURG = flagURG;
	}

	public boolean hasFlagECE() {
		return flagECE;
	}

	public void setFlagECE(boolean flagECE) {
		this.flagECE = flagECE;
	}

	public boolean hasFlagSYN() {
		return flagSYN;
	}

	public void setFlagSYN(boolean flagSYN) {
		this.flagSYN = flagSYN;
	}

	public boolean hasFlagACK() {
		return flagACK;
	}

	public void setFlagACK(boolean flagACK) {
		this.flagACK = flagACK;
	}

	public boolean hasFlagCWR() {
		return flagCWR;
	}

	public void setFlagCWR(boolean flagCWR) {
		this.flagCWR = flagCWR;
	}

	public boolean hasFlagRST() {
		return flagRST;
	}

	public void setFlagRST(boolean flagRST) {
		this.flagRST = flagRST;
	}

	public int getTCPWindow(){
		return TCPWindow;
	}

	public void setTCPWindow(int TCPWindow){
		this.TCPWindow = TCPWindow;
	}
}

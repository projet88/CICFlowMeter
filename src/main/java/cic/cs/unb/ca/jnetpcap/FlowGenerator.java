package cic.cs.unb.ca.jnetpcap;

import cic.cs.unb.ca.jnetpcap.worker.FlowGenListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import static cic.cs.unb.ca.jnetpcap.Utils.LINE_SEP;

/**
 * Generate flow, by adding pakets on the right flow according to flowtimout and activitytimout
 */
public class FlowGenerator {
    public static final Logger logger = LoggerFactory.getLogger(FlowGenerator.class);
    //total 85 colums
	/*public static final String timeBasedHeader = "Flow ID, Source IP, Source Port, Destination IP, Destination Port, Protocol, "
			+ "Timestamp, Flow Duration, Total Fwd Packets, Total Backward Packets,"
			+ "Total Length of Fwd Packets, Total Length of Bwd Packets, "
			+ "Fwd Packet Length Max, Fwd Packet Length Min, Fwd Packet Length Mean, Fwd Packet Length Std,"
			+ "Bwd Packet Length Max, Bwd Packet Length Min, Bwd Packet Length Mean, Bwd Packet Length Std,"
			+ "Flow Bytes/s, Flow Packets/s, Flow IAT Mean, Flow IAT Std, Flow IAT Max, Flow IAT Min,"
			+ "Fwd IAT Total, Fwd IAT Mean, Fwd IAT Std, Fwd IAT Max, Fwd IAT Min,"
			+ "Bwd IAT Total, Bwd IAT Mean, Bwd IAT Std, Bwd IAT Max, Bwd IAT Min,"
			+ "Fwd PSH Flags, Bwd PSH Flags, Fwd URG Flags, Bwd URG Flags, Fwd Header Length, Bwd Header Length,"
			+ "Fwd Packets/s, Bwd Packets/s, Min Packet Length, Max Packet Length, Packet Length Mean, Packet Length Std, Packet Length Variance,"
			+ "FIN Flag Count, SYN Flag Count, RST Flag Count, PSH Flag Count, ACK Flag Count, URG Flag Count, "
			+ "CWE Flag Count, ECE Flag Count, Down/Up Ratio, Average Packet Size, Avg Fwd Segment Size, Avg Bwd Segment Size, Fwd Header Length,"
			+ "Fwd Avg Bytes/Bulk, Fwd Avg Packets/Bulk, Fwd Avg Bulk Rate, Bwd Avg Bytes/Bulk, Bwd Avg Packets/Bulk,"
			+ "Bwd Avg Bulk Rate,"
			+ "Subflow Fwd Packets, Subflow Fwd Bytes, Subflow Bwd Packets, Subflow Bwd Bytes,"
			+ "Init_Win_bytes_forward, Init_Win_bytes_backward, act_data_pkt_fwd, min_seg_size_forward,"
			+ "Active Mean, Active Std, Active Max, Active Min,"
			+ "Idle Mean, Idle Std, Idle Max, Idle Min, Label";*/

    //40/86
    private FlowGenListener mListener;
    private HashMap<String, BasicFlow> currentFlows; // list of flows in process
    private HashMap<Integer, BasicFlow> finishedFlows; // list of flows already processed
    private HashMap<String, ArrayList> IPAddresses;

    private boolean bidirectional; // true if the flow are considered as bidirectional
    private long flowTimeOut; // Timeout of flows
    private long flowActivityTimeOut; // timout of flow Activity
    private int finishedFlowCount;

    /**
     * Constructor of the class
     *
     * @param bidirectional   boolean to know if the flows are considered bidirectional
     * @param flowTimeout     timeout of the flows
     * @param activityTimeout timout for the activity of flows
     */
    public FlowGenerator(boolean bidirectional, long flowTimeout, long activityTimeout) {
        super();
        this.bidirectional = bidirectional;
        this.flowTimeOut = flowTimeout;
        this.flowActivityTimeOut = activityTimeout;
        init();
    }

    /**
     * initialise currentFlows,finishedFlows,IPAddresses,finishedFlowCount at 0
     */
    private void init() {
        currentFlows = new HashMap<>();
        finishedFlows = new HashMap<>();
        IPAddresses = new HashMap<>();
        finishedFlowCount = 0;
    }

    /**
     * The listener of this flowGenerator become listener
     *
     * @param listener for a flowGenerator
     */
    public void addFlowListener(FlowGenListener listener) {
        mListener = listener;
    }

    /**
     * add the packet to it's flow in the flow generator
     * end the flow if needed
     * add a new flow if needed
     * @see #getFlowCount()
     * @param packet to be added
     */
    public void addPacket(BasicPacketInfo packet) {
        if (packet == null) {
            return;
        }
        BasicFlow flow; // flow we will use
        long currentTimestamp = packet.getTimeStamp();
        String id; // will be the id of the flow

        // if the packet is in the packet list (in the forward direction)
        boolean packetCurrentFwd = this.currentFlows.containsKey(packet.fwdFlowId());
        /* check if we already process the flow of this packet in forward or backward direction  */
        if (packetCurrentFwd || this.currentFlows.containsKey(packet.bwdFlowId())) {
            /* get the flow id */
            if (packetCurrentFwd) {
                id = packet.fwdFlowId(); //
            } else {
                id = packet.bwdFlowId();
            }
            flow = currentFlows.get(id);

            /* process the paket flow */

            // Flow finished due flowtimeout:
            // 1.- we move the flow to finished flow list
            // 2.- we eliminate the flow from the current flow list
            // 3.- we create a new flow with the packet-in-process
            if ((currentTimestamp - flow.getFlowStartTime()) > flowTimeOut) { // if the flow has to finish due to timeout
                if (flow.packetCount() > 1) { //if the flow contain at least 1 packet
                    if (mListener != null) {
                        mListener.onFlowGenerated(flow);
                    } else {
                        finishedFlows.put(getFlowCount(), flow);
                    }
                    //flow.endActiveIdleTime(currentTimestamp,this.flowActivityTimeOut, this.flowTimeOut, false);
                }
                currentFlows.remove(id); //eliminate the flow from the current flow list
                /*create a new flow with the packet-in-process*/
                currentFlows.put(id, new BasicFlow(bidirectional, packet, flow.getSrc(), flow.getDst(), flow.getSrcPort(), flow.getDstPort()));

                int cfsize = currentFlows.size();
                if (cfsize % 50 == 0) {
                    logger.debug("Timeout current has {} flow", cfsize);
                }
                // Flow finished due FIN flag (tcp only):
                // 1.- we add the packet-in-process to the flow (it is the last packet)
                // 2.- we move the flow to finished flow list
                // 3.- we eliminate the flow from the current flow list
            } else if (packet.hasFlagFIN()) {
                logger.debug("FlagFIN current has {} flow", currentFlows.size());
                flow.addPacket(packet); // add the packet-in-process to the flow
                if (mListener != null) {
                    mListener.onFlowGenerated(flow);
                } else {
                    finishedFlows.put(getFlowCount(), flow); //move the flow to finished flow list
                }
                currentFlows.remove(id); //eliminate the flow from the current flow list
                // Flow not finished  :
                // 1- we add the packet-in-process to the flow
            } else {
                flow.updateActiveIdleTime(currentTimestamp, this.flowActivityTimeOut); // update the active time to not trigger the activeTimout
                flow.addPacket(packet); // add the packet to the flow
                currentFlows.put(id, flow);
            }
        } else {
            currentFlows.put(packet.fwdFlowId(), new BasicFlow(bidirectional, packet)); //create a new flow with the packet-in-process
        }
    }

    /*public void dumpFlowBasedFeatures(String path, String filename,String header){
    	BasicFlow   flow;
    	try {
    		System.out.println("TOTAL Flows: "+(finishedFlows.size()+currentFlows.size()));
    		FileOutputStream output = new FileOutputStream(new File(path+filename));    
    		
    		output.write((header+"\n").getBytes());
    		Set<Integer> fkeys = finishedFlows.keySet();    		
			for(Integer key:fkeys){
	    		flow = finishedFlows.get(key);
	    		if(flow.packetCount()>1)				
	    			output.write((flow.dumpFlowBasedFeaturesEx()+"\n").getBytes());
			}
			Set<String> ckeys = currentFlows.keySet();   		
			for(String key:ckeys){
	    		flow = currentFlows.get(key);
	    		if(flow.packetCount()>1)				
	    			output.write((flow.dumpFlowBasedFeaturesEx()+"\n").getBytes());
			}			
			
			output.flush();
			output.close();			
		} catch (IOException e) {
			e.printStackTrace();
		}

    }*/

    /**
     * Allow to generate the list of Features for the finished and unfinished flows in this flow generator
     *
     * @param path     path to the output file
     * @param filename filename of the output
     * @param header   header of the file
     * @return the number of flows processed
     */
    public int dumpLabeledFlowBasedFeatures(String path, String filename, String header) {
        BasicFlow flow;
        int total = 0; // number of flows processed
        int zeroPkt = 0; // number of flows with 0 packet
        try {
            //total = finishedFlows.size()+currentFlows.size(); becasue there are 0 packet BasicFlow in the currentFlows
            FileOutputStream output = new FileOutputStream(new File(path + filename)); // creates an oupout stream for the output
            logger.debug("dumpLabeledFlow: ", path + filename);
            output.write((header + "\n").getBytes());
            Set<Integer> fkeys = finishedFlows.keySet(); // list of the keys of finished flows
            for (Integer key : fkeys) {
                flow = finishedFlows.get(key); // get the flow
                if (flow.packetCount() > 1) { // if the flow not empty
                    output.write((flow.dumpFlowBasedFeaturesEx() + "\n").getBytes()); // write the features of the flow
                    total++; // add 1 to the total of flow
                } else {
                    zeroPkt++;
                }
            }
            logger.debug("dumpLabeledFlow finishedFlows -> {},{}", zeroPkt, total);

            Set<String> ckeys = currentFlows.keySet(); // list of key of flow that was in process (none finished)
            output.write((header + "\n").getBytes());
            for (String key : ckeys) {
                flow = currentFlows.get(key);
                if (flow.packetCount() > 1) {
                    output.write((flow.dumpFlowBasedFeaturesEx() + "\n").getBytes());
                    total++;
                } else {
                    zeroPkt++;
                }

            }
            logger.debug("dumpLabeledFlow total(include current) -> {},{}", zeroPkt, total);
            output.flush();
            output.close();
        } catch (IOException e) {

            logger.debug(e.getMessage());
        }

        return total;
    }

    /**
     *Allow to generate the list of Features for the unfinished flow in this flow generator
     * @param fileFullPath path of the file
     * @param header header
     * @return the number of flows processed
     */
    public long dumpLabeledCurrentFlow(String fileFullPath, String header) {
        if (fileFullPath == null || header == null) {
            String ex = String.format("fullFilePath=%s,filename=%s", fileFullPath);
            throw new IllegalArgumentException(ex);
        }
        /* output treatment */
        File file = new File(fileFullPath);
        FileOutputStream output = null;
        int total = 0;
        try {
            if (file.exists()) {
                output = new FileOutputStream(file, true);
            } else {
                if (file.createNewFile()) {
                    output = new FileOutputStream(file);
                    output.write((header + LINE_SEP).getBytes());
                }
            }

            for (BasicFlow flow : currentFlows.values()) {
                if (flow.packetCount() > 1) {
                    output.write((flow.dumpFlowBasedFeaturesEx() + LINE_SEP).getBytes());
                    total++;
                } else {

                }
            }

        } catch (IOException e) {
            logger.debug(e.getMessage());
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException e) {
                logger.debug(e.getMessage());
            }
        }
        return total;
    }

    /**
     * add 1 to the flowCount and return the FlowCount
     * @return the FlowCount
     */
    private int getFlowCount() {
        this.finishedFlowCount++;
        return this.finishedFlowCount;
    }
}

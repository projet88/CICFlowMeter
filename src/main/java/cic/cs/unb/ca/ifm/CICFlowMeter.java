package cic.cs.unb.ca.ifm;

import java.io.File;
import java.io.FilenameFilter;

import cic.cs.unb.ca.jnetpcap.FlowFeature;
import org.jnetpcap.PcapClosedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cic.cs.unb.ca.jnetpcap.BasicPacketInfo;
import cic.cs.unb.ca.jnetpcap.FlowGenerator;
import cic.cs.unb.ca.jnetpcap.PacketReader;


public class CICFlowMeter {

    public static final Logger logger = LoggerFactory.getLogger(CICFlowMeter.class);

    public static void main(String[] args) {

        PacketReader packetReader; // extract info from a packet
        BasicPacketInfo basicPacket = null; // info of the packet
        FlowGenerator flowGen; //15000 useconds = 15ms

        boolean readIP6 = false; // boolean to know if we read IPV6 IP
        boolean readIP4 = true; // boolean to know if we read IPV4 IP
        // readIP6 != readIP4
        int totalFlows = 0; //number of Flows generated by the flowGenerator

        String rootPath = System.getProperty("user.dir"); // path of the root
        String pcapPath; // path to the pcap we want to analyse
        String outpath; // path to the output

        /* Select path for reading all .pcap files */
        if (args.length < 1 || args[0] == null) {
            pcapPath = rootPath + "/data/in/";
        } else {
            pcapPath = args[0];
        }

        /* Select path for writing all .csv files */
        if (args.length < 2 || args[1] == null) {
            outpath = rootPath + "/data/out/";
        } else {
            outpath = args[1];
        }

        //String[] files = new File(pcapPath).list();


        String[] pcapfiles = new File(pcapPath).list(new FilenameFilter() {
            /**
             * return true if the file have pcap extension
             * @param dir directory of the file
             * @param name name of the file
             * @return
             */
            @Override
            public boolean accept(File dir, String name) {
                return (name.toLowerCase().endsWith("pcap"));
            }
        });

        if (pcapfiles == null || pcapfiles.length == 0) {
            logger.info("Sorry,no pcap files can be found under: {}", pcapPath);
            return;
        }

        logger.info("");
        logger.info("CICFlowMeterV2 found: {} Files.", pcapfiles.length);

        for (String file : pcapfiles) {
            flowGen = new FlowGenerator(true, 120000000L, 5000000L);// create a flow generator
            packetReader = new PacketReader(pcapPath + file, readIP4, readIP6);
            logger.info("");
            logger.info("");
            logger.info("Working on... {} ", file);

            int nValid = 0; // number of valid packets
            int nTotal = 0; // number of packets
            int nDiscarded = 0; // number of discarded packets

            long start = System.currentTimeMillis();
            /* browse all the packets on packetReader */
            while (true) {
                try {
                    basicPacket = packetReader.nextPacket(); // get the next packet
                    nTotal++;
                    if (basicPacket != null) { // null if packetReader failed to read it
                        flowGen.addPacket(basicPacket); // give the packet to the flow generator
                        nValid++; // the packet is valid
                    } else {
                        nDiscarded++; // the packet cannot be read
                    }
                } catch (PcapClosedException e) {
                    break;
                }
            }
            // graphical info output
            long end = System.currentTimeMillis();
            logger.info("Done! in {} seconds", ((end - start) / 1000));
            logger.info("\t Total packets: {}", nTotal);
            logger.info("\t Valid packets: {}", nValid);
            logger.info("\t Ignored packets:{} {} ", nDiscarded, (nTotal - nValid));
            logger.info("PCAP duration {} seconds", ((packetReader.getLastPacket() - packetReader.getFirstPacket()) / 1000));
            logger.info("----------------------------------------------------------------------------");
            totalFlows += flowGen.dumpLabeledFlowBasedFeatures(outpath, file + "_ISCX.csv", FlowFeature.getHeader());
            //flowGen.dumpIPAddresses(outpath, file+"_IP-Addresses.csv");
            //flowGen.dumpTimeBasedFeatures(outpath, file+".csv");

        }
        logger.info("\n\n----------------------------------------------------------------------------\n TOTAL FLOWS GENERATED: {}", totalFlows);
        logger.info("----------------------------------------------------------------------------\n");
//		try {
//			Files.write(Paths.get("src/main/resources/executionLog.log"),flowGen.dumpFlows().getBytes());
//		} catch (IOException e) {			
//			e.printStackTrace();
//		}
    }
}
